.PHONY: clean

test = test.h test.cpp

all: slacks profile 

slacks: main.o circuit.o
	g++ -g circuit.o main.o  -o main

main.o: main.cpp
	g++ -c main.cpp

circuit.o: circuit.cpp
	g++ -c circuit.cpp

profile: creating_cerc.cpp
	g++ -g creating_cerc.cpp -o profile

testall: $(test)
	g++ $(test) circuit.cpp -lcppunit -o testall

clean:
	rm -f main
	rm -f profile
	rm -rf ./test
	rm -f *.o




