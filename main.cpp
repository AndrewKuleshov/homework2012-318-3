/*
 * Copyright KuleshovAndrew.
 * C++ program that creates circuits
 */

#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include "circuit.cpp"


int main()
{
    Graph graph(".nodes", ".nets", ".aux");
    graph.compute_aat();
    graph.compute_rat();
    graph.compute_slack();
    graph.print_answer(".slacks", ".result");

    return 0;
}
