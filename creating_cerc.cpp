/*
 * Copyright KuleshovAndrew.
 * C++ program that creates circuits
 */
#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <cstdlib>
# define vec pair <int,int>

/**
 * Main program.
 *
 * @author Andrew Kuleshov
 * @file creating_cerc.cpp
 * @since 9.12.2012
 */

using namespace std;

int main(int argc, char *argv[]){

    if (argc < 3) {
        cout<<"Input all parameters!"<<endl;
        exit(-1);
    }

    int n, m;
    ofstream nodes("./.nodes"), nets("./.nets"), aux("./.aux");

    n = strtol(argv[1], NULL, 10);
    m = strtol(argv[2], NULL, 10);

    srand(time(NULL));
    vector <vec> possible_edges;
    vector <vec> edges;
    vector <int> node_in(n, 0);
    vector <int> node_out(n, 0);
    vec edge;
    int edge_number = 0;

    int i, j;
    for (i = 0; i < n; i++) {
        for (j = i + 1; j < n; j++) {
            edge.first = i;
            edge.second = j;

            edges.push_back(edge);
            possible_edges.push_back(edge);
            node_in[j]++;

            node_out[i]++;
            edge_number++;
        }
    }

    edge_number -= m;
    while(edge_number) {

        int i = 0;
        while (i < possible_edges.size()) {
        if (node_in[possible_edges[i].first] == 0 && node_out[possible_edges[i].first] == 1) {
            possible_edges.erase(possible_edges.begin() + i);
        } else if (node_in[possible_edges[i].second] == 1 && node_out[possible_edges[i].second] == 0) {
            possible_edges.erase(possible_edges.begin() + i);
        } else {
            i++;
            }
        }


        if (possible_edges.empty()) {
            exit(-1);
        }
        i = (rand() / (RAND_MAX + 1.0) * possible_edges.size());

        node_out[possible_edges[i].first]--;
        node_in[possible_edges[i].second]--;


        for (j = 0; j < edges.size(); j++) {
            if (edges[j].first == possible_edges[i].first && edges[j].second == possible_edges[i].second) {
                edges.erase(edges.begin() + j);
                break;
            }
        }
        possible_edges.erase(possible_edges.begin() + i);
        edge_number--;
    }


    for (i = 0; i < n; i++) {
        nodes << ((double)rand() / (RAND_MAX + 1.0) * 100) << endl;
    }

    for (i = 0; i < edges.size(); i++) {
        nets << edges[i].first << " " << edges[i].second << " " << ((double)rand() / (RAND_MAX + 1.0) * 20) << endl;
    }

    for (i = 0; i < n; i++) {
        if (node_out[i] == 0) {
            aux << i << " " << ((double)rand() / (RAND_MAX + 1.0) * 250) << endl;
        }
    }

    nodes.close();
    nets.close();
    aux.close();
    return 0;
}
