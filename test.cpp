/*
 * Copyright KuleshovAndrew.
 * C++ program that creates circuits
 */

#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include "test.h"

/**
 * Implementation of class of test
 * @ author Andrew Kuleshov
 */



CPPUNIT_TEST_SUITE_REGISTRATION(TestCircuit);
void TestCircuit::setUp()
{


}
void TestCircuit::tearDown()
{
}

void TestCircuit::createFiles(vector <int> &nodes_delay, vector <int> &start_edge, vector <int> &end_edge, vector <int> edge_delay, vector <int> &out_nodes, vector <int> &out_delay)
{
    ofstream nodes("./.nodes"), nets("./.nets"), aux("./.aux");
    int i;

    for (i = 0; i < nodes_delay.size(); i++) {
        nodes << nodes_delay[i] << endl;
    }

    for (i = 0; i < start_edge.size(); i++) {
        nets << start_edge[i] << " " << end_edge[i] << " " << edge_delay[i] << endl;
    }

    for (i = 0; i < out_nodes.size(); i++) {
        aux << out_nodes[i] << " " << out_delay[i] << endl;
    }

    nodes.close();
    nets.close();
    aux.close();
}

void TestCircuit::testAAT1()
{
    vector <int> nodes_delay(3, 0);
    nodes_delay[0] = 0;
    nodes_delay[1] = 1;
    nodes_delay[2] = 2;

    vector <int> start_edge(2, 0);
    vector <int> end_edge(2, 0);
    vector <int> edge_delay(2, 0);
    start_edge[0] = 0;    end_edge[0] = 1;    edge_delay[0] = 12;
    start_edge[1] = 0;    end_edge[1] = 2;    edge_delay[1] = 10;


    vector <int> out_nodes(2, 0);
    out_nodes[0] = 1;
    out_nodes[1] = 2;

    vector <int> out_delay(2, 0);
    out_delay[0] = 5;
    out_delay[1] = 6;

    createFiles(nodes_delay, start_edge, end_edge, edge_delay, out_nodes, out_delay);

    Graph graph(".nodes", ".nets", ".aux");
    graph.compute_aat();

    vector <double> test_vec(3, 0);
    test_vec[0] = 0; test_vec[1] = 13; test_vec[2] = 12;

    CPPUNIT_ASSERT(test_vec == graph.get_aat());
}

void TestCircuit::testRAT1()
{
    vector <int> nodes_delay(3, 0);
    nodes_delay[0] = 0; nodes_delay[1] = 1;
    nodes_delay[2] = 2;

    vector <int> start_edge(2, 0);
    vector <int> end_edge(2, 0);
    vector <int> edge_delay(2, 0);
    start_edge[0] = 0;    end_edge[0] = 1;    edge_delay[0] = 12;
    start_edge[1] = 0;    end_edge[1] = 2;    edge_delay[1] = 10;


    vector <int> out_nodes(2, 0);
    out_nodes[0] = 1;
    out_nodes[1] = 2;

    vector <int> out_delay(2, 0);
    out_delay[0] = 5;
    out_delay[1] = 6;

    createFiles(nodes_delay, start_edge, end_edge, edge_delay, out_nodes, out_delay);

    Graph graph(".nodes", ".nets", ".aux");
    graph.compute_rat();

    vector <double> test_vec(3, 0);
    test_vec[0] = -4; test_vec[1] = 5; test_vec[2] = 6;

    CPPUNIT_ASSERT(test_vec == graph.get_rat());
}


void TestCircuit::testSLACK1()
{
    vector <int> nodes_delay(3, 0);
    nodes_delay[0] = 0; nodes_delay[1] = 1;
    nodes_delay[2] = 2;

    vector <int> start_edge(2, 0);
    vector <int> end_edge(2, 0);
    vector <int> edge_delay(2, 0);
    start_edge[0] = 0;    end_edge[0] = 1;    edge_delay[0] = 12;
    start_edge[1] = 0;    end_edge[1] = 2;    edge_delay[1] = 10;


    vector <int> out_nodes(2, 0);
    out_nodes[0] = 1;
    out_nodes[1] = 2;

    vector <int> out_delay(2, 0);
    out_delay[0] = 5;
    out_delay[1] = 6;

    createFiles(nodes_delay, start_edge, end_edge, edge_delay, out_nodes, out_delay);

    Graph graph(".nodes", ".nets", ".aux");
    graph.compute_aat();
    graph.compute_rat();
    graph.compute_slack();

    vector <double> test_vec(3, 0);
    test_vec[0] = -4; test_vec[1] = -8; test_vec[2] = -6;

    CPPUNIT_ASSERT(test_vec == graph.get_slack());
}

void TestCircuit::testAAT2()
{
    vector <int> nodes_delay(6, 0);
    nodes_delay[0] = 0;
    nodes_delay[1] = 1;
    nodes_delay[2] = 2;
    nodes_delay[3] = 3;
    nodes_delay[4] = 4;
    nodes_delay[5] = 5;

    vector <int> start_edge(6, 0);
    vector <int> end_edge(6, 0);
    vector <int> edge_delay(6, 0);
    start_edge[0] = 0;    end_edge[0] = 2;    edge_delay[0] = 6;
    start_edge[1] = 0;    end_edge[1] = 4;    edge_delay[1] = 7;
    start_edge[2] = 1;    end_edge[2] = 3;    edge_delay[2] = 4;
    start_edge[3] = 2;    end_edge[3] = 4;    edge_delay[3] = 5;
    start_edge[4] = 2;    end_edge[4] = 5;    edge_delay[4] = 11;
    start_edge[5] = 3;    end_edge[5] = 5;    edge_delay[5] = 9;


    vector <int> out_nodes(2, 0);
    out_nodes[0] = 4;
    out_nodes[1] = 5;

    vector <int> out_delay(2, 0);
    out_delay[0] = 30;
    out_delay[1] = 40;

    createFiles(nodes_delay, start_edge, end_edge, edge_delay, out_nodes, out_delay);

    Graph graph(".nodes", ".nets", ".aux");
    graph.compute_aat();

    vector <double> test_vec(6, 0);
    test_vec[0] = 0;
    test_vec[1] = 1;
    test_vec[2] = 8;
    test_vec[3] = 8;
    test_vec[4] = 17;
    test_vec[5] = 24;

    CPPUNIT_ASSERT(test_vec == graph.get_aat());
}

void TestCircuit::testRAT2()
{
    vector <int> nodes_delay(6, 0);
    nodes_delay[0] = 0;
    nodes_delay[1] = 1;
    nodes_delay[2] = 2;
    nodes_delay[3] = 3;
    nodes_delay[4] = 4;
    nodes_delay[5] = 5;

    vector <int> start_edge(6, 0);
    vector <int> end_edge(6, 0);
    vector <int> edge_delay(6, 0);
    start_edge[0] = 0;    end_edge[0] = 2;    edge_delay[0] = 6;
    start_edge[1] = 0;    end_edge[1] = 4;    edge_delay[1] = 7;
    start_edge[2] = 1;    end_edge[2] = 3;    edge_delay[2] = 4;
    start_edge[3] = 2;    end_edge[3] = 4;    edge_delay[3] = 5;
    start_edge[4] = 2;    end_edge[4] = 5;    edge_delay[4] = 11;
    start_edge[5] = 3;    end_edge[5] = 5;    edge_delay[5] = 9;


    vector <int> out_nodes(2, 0);
    out_nodes[0] = 4;
    out_nodes[1] = 5;

    vector <int> out_delay(2, 0);
    out_delay[0] = 30;
    out_delay[1] = 40;

    createFiles(nodes_delay, start_edge, end_edge, edge_delay, out_nodes, out_delay);

    Graph graph(".nodes", ".nets", ".aux");
    graph.compute_rat();

    vector <double> test_vec(6, 0);
    test_vec[0] = 23;
    test_vec[1] = 23;
    test_vec[2] = 27;
    test_vec[3] = 28;
    test_vec[4] = 30;
    test_vec[5] = 40;

    CPPUNIT_ASSERT(test_vec == graph.get_rat());
}

void TestCircuit::testSLACK2()
{
    vector <int> nodes_delay(6, 0);
    nodes_delay[0] = 0;
    nodes_delay[1] = 1;
    nodes_delay[2] = 2;
    nodes_delay[3] = 3;
    nodes_delay[4] = 4;
    nodes_delay[5] = 5;

    vector <int> start_edge(6, 0);
    vector <int> end_edge(6, 0);
    vector <int> edge_delay(6, 0);
    start_edge[0] = 0;    end_edge[0] = 2;    edge_delay[0] = 6;
    start_edge[1] = 0;    end_edge[1] = 4;    edge_delay[1] = 7;
    start_edge[2] = 1;    end_edge[2] = 3;    edge_delay[2] = 4;
    start_edge[3] = 2;    end_edge[3] = 4;    edge_delay[3] = 5;
    start_edge[4] = 2;    end_edge[4] = 5;    edge_delay[4] = 11;
    start_edge[5] = 3;    end_edge[5] = 5;    edge_delay[5] = 9;


    vector <int> out_nodes(2, 0);
    out_nodes[0] = 4;
    out_nodes[1] = 5;

    vector <int> out_delay(2, 0);
    out_delay[0] = 30;
    out_delay[1] = 40;

    createFiles(nodes_delay, start_edge, end_edge, edge_delay, out_nodes, out_delay);

    Graph graph(".nodes", ".nets", ".aux");
    graph.compute_aat();
    graph.compute_rat();
    graph.compute_slack();

    vector <double> test_vec(6, 0);
    test_vec[0] = 23;
    test_vec[1] = 22;
    test_vec[2] = 19;
    test_vec[3] = 20;
    test_vec[4] = 13;
    test_vec[5] = 16;

    CPPUNIT_ASSERT(test_vec == graph.get_slack());
}


int main( int argc, char **argv)
{
    CppUnit::TextUi::TestRunner runner;
    CppUnit::TestFactoryRegistry &registry
    = CppUnit::TestFactoryRegistry::getRegistry();
    runner.addTest(registry.makeTest());
    runner.run();
    return 0;
}
