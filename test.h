/*
 * Copyright Andrew Kuleshov 2012.
 * C++ program computing slack for circuit graph.
 */

#include <cppunit/extensions/HelperMacros.h>
#include "circuit.cpp"

/**
 * Test class TestCircuit for class Slacks.
 * @author Andrew Kuleshov
 * @file slacks-test.h
 * @since 16.12.2012
 */

class TestCircuit : public CPPUNIT_NS::TestFixture
{
    CPPUNIT_TEST_SUITE(TestCircuit);
    CPPUNIT_TEST(testAAT1);
    CPPUNIT_TEST(testRAT1);
    CPPUNIT_TEST(testSLACK1);
    CPPUNIT_TEST(testAAT2);
    CPPUNIT_TEST(testRAT2);
    CPPUNIT_TEST(testSLACK2);
    CPPUNIT_TEST_SUITE_END();
public:
    void setUp();
    void tearDown();
    void createFiles(vector <int> &nodes_delay, vector <int> &start_edge, vector <int> &end_edge, vector <int> edge_delay, vector <int> &out_nodes, vector <int> &out_delay);
protected:
    void testAAT1();
    void testRAT1();
    void testSLACK1();
    void testAAT2();
    void testRAT2();
    void testSLACK2();

private:

};

