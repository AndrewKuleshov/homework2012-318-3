/*
 * Copyright KuleshovAndrew.
 * C++ program that counts slacks.
 */

#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <fstream>
#include <cstdio>
#include <cstdlib>

using namespace std;

/**
 * Main program.
 *
 * @author Andrew Kuleshov
 * @file main.cpp
 * @since 02.12.2012
 */

#define Node pair <vector <int>,vector <int> >


class Graph
{

vector <Node> nodes;
vector <double> aat;
vector <double> rat;
vector <double> slack;
vector <double> nodes_delay;
map <pair <int, int>, double> edges_delay;

public:


    /**
    *Constructor
    *@param node_file  information about nodes
    *@param edge_file  information about edges
    *@param rat_file   information about rat
    */
    Graph (string node_file, string edge_file, string rat_file){
    double delay;

    ifstream node_stream(node_file.c_str()), edge_stream(edge_file.c_str()), rat_stream(rat_file.c_str());
    if (!node_stream) {
        cout << "Cannot open file to read : .nodes" << endl;
        exit (1);
    }

    if (!edge_stream) {
        cout << "Cannot open file to read : .nets" << endl;
        exit (1);
    }

    if (!rat_stream) {
        cout << "Cannot open file to read : .rat" << endl;
        exit (1);
    }

    while (node_stream >> delay) {
        nodes_delay.push_back(delay);
    }
    node_stream.close();

    nodes.resize(nodes_delay.size());
    rat.resize(nodes_delay.size());
    aat.resize(nodes_delay.size());
    slack.resize(nodes_delay.size());


    int v1, v2;
    while (edge_stream >> v1 >> v2 >> delay) {
        pair <int, int> edge(v1, v2);
        edges_delay[edge] = delay;
        nodes[v1].first.push_back(v2);
        nodes[v2].second.push_back(v1);
    }
    edge_stream.close();

    double rat_init;
    while (rat_stream >> v1 >> rat_init) {
        rat[v1] = rat_init;
    }
    rat_stream.close();
}






   /**
    *Compute AAT of node
    *@param index node to process
    *@return AAT of current node
    */

    double compute_node_aat(int index){
    int i;
    double max, curr;
    for (i = 0; i < nodes[index].second.size(); i++) {
        pair <int, int> edge(nodes[index].second[i], index);
        curr = aat[nodes[index].second[i]] + edges_delay[edge];
        if (curr > max || !i) {
            max = curr;
        }
    }
    return max + nodes_delay[index];
    }



   /**
    *Compute RAT in all nodes and write them in rat
    */
    void compute_aat(){
    vector <int> flags(nodes.size(), 0);
    int i, j;
    for (i = 0; i < nodes.size(); i++) {
        if (nodes[i].second.empty()) {
            flags[i] = 1;
            aat[i] = nodes_delay[i];
        }
    }

    bool ability;
    while(test_flags(flags)) {
        for (i = 0; i < nodes.size(); i++) {
            if (!flags[i]) {
                ability = true;
                for (j = 0; j < nodes[i].second.size(); j++) {
                    if (!flags[nodes[i].second[j]]) {
                        ability = false;
                        break;
                    }
                }
                if (ability) {
                    aat[i] = compute_node_aat(i);
                    flags[i] = 1;
                }
            }
        }
    }
}

vector <double> get_aat(){
    return aat;
}


vector <double> get_rat(){
    return rat;
}



vector <double> get_slack(){
    return slack;
}


double compute_node_rat(int index){
    int i;
    double max, curr;
    for (i = 0; i < nodes[index].first.size(); i++) {
        pair <int, int> edge(index, nodes[index].first[i]);
        curr = rat[nodes[index].first[i]] - edges_delay[edge];
        if (curr > max || !i) {
            max = curr;
        }
    }
    return max - nodes_delay[index];
}



void compute_rat(){
    vector <int> flags(nodes.size(), 0);
    int i, j;
    for (i = 0; i < nodes.size(); i++) {
        if (nodes[i].first.empty()) {
            flags[i] = 1;
        }
    }

    bool ability;
    while(test_flags(flags)) {
        for (i = 0; i < nodes.size(); i++) {
            if (!flags[i]) {
                ability = true;
                for (j = 0; j < nodes[i].first.size(); j++) {
                    if (!flags[nodes[i].first[j]]) {
                        ability = false;
                        break;
                    }
                }
                if (ability) {
                    rat[i] = compute_node_rat(i);
                    flags[i] = 1;
                }
            }
        }
    }
}




/**
 * Compute SLACK
 */
void compute_slack()
{
    int i;
    for (i = 0; i < nodes.size(); i++) {
        slack[i] = rat[i] - aat[i];
    }
}


   /**
    *Print result of program in files
    *@param file_slacks file where to print all slacks
    *@param file_result file where to print nodes with negative slack
    */

    void print_answer(string file_slacks, string file_result)
{
    int i;
    ofstream slacks_stream(file_slacks.c_str()), result_stream(file_result.c_str());
    vector <int> negative_slacks;
    if (! slacks_stream) {
        string error = "Cannot open file to print " ;
        exit (1);
    }

    if (!result_stream) {
        string error = "Cannot open file to print " ;
        exit (1);
    }
    for (i = 0; i < nodes.size(); i++) {
        slacks_stream << slack[i] << endl;
        if (slack[i] < 0) {
            negative_slacks.push_back(i);
        }
    }
    slacks_stream.close();

    if (negative_slacks.empty()) {
        result_stream << "0" << endl;
    } else {
        result_stream << "1" << endl;
    }
    for (i = 0; i < negative_slacks.size(); i++) {
        result_stream << negative_slacks[i] << " ";
    }
    result_stream << endl;
    result_stream.close();	

}

bool test_flags(vector <int> &flags)
{
    int i;
    for (i = 0; i < flags.size(); i++) {
        if (!flags[i]) return true;
    }
    return false;
}

};


